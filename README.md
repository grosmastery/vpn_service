### **Step 1: Clone the Project Repository**

Clone the project repository from GitLab to your local machine:

```
git clone https://gitlab.com/grosmastery/vpn_service.git
cd <project_folder>
```

### **Step 2: Docker Configuration**

Ensure your project contains the following files:

- Dockerfile
- docker-compose.yml
- requirements.txt
- Django project files
- entrypoint.sh

### **Step 3: Build and Run the Docker Container**

Run the following commands to build and start the Docker containers:

```
docker-compose up --build -d  # Build and start the Docker
```

This will build the Docker images based on the `Dockerfile` and start the services defined in `docker-compose.yml`.
