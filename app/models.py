from django.db import models
from django.contrib.auth.models import User

class UserSite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    url = models.URLField()

    def __str__(self):
        return self.name

class TrafficStats(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    site = models.ForeignKey(UserSite, on_delete=models.CASCADE)
    visits = models.IntegerField(default=0)
    data_transferred = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.user.username} - {self.site.name}"
