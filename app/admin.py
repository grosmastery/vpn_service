from django.contrib import admin
from .models import UserSite, TrafficStats

admin.site.register(UserSite)
admin.site.register(TrafficStats)
