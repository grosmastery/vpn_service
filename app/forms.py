from django import forms
from django.contrib.auth import get_user_model
from app.models import UserSite

class SignUpForm(forms.ModelForm):
    check_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = ("email", "username","password", "check_password")
        widgets = {
            "password": forms.widgets.PasswordInput(),
        }

    def clean_check_password(self):
        if self.cleaned_data["password"] != self.cleaned_data["check_password"]:
            raise forms.ValidationError("Invalid Password")
        else:
            return self.cleaned_data["password"]

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user
    
    
class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ("username",)


class UserSiteForm(forms.ModelForm):
    class Meta:
        model = UserSite
        fields = ('name', 'url',)