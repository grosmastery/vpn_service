from django.shortcuts import render, redirect
from django.views import View 
from django.views.generic import TemplateView, FormView, CreateView
from app.models import UserSite, TrafficStats
from bs4 import BeautifulSoup
import requests
from django.http import HttpResponse
from app.forms import SignUpForm, UpdateProfileForm, UserSiteForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class SignUpView(FormView):
    template_name = "signup.html"
    form_class = SignUpForm
    success_url = "/login/"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
    

class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["traffic_stats"] = TrafficStats.objects.filter(user=self.request.user)
        return context

    def post(self, request, *args, **kwargs):
        form = UpdateProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')
        return self.render_to_response(self.get_context_data(form=form))
    

class UserSiteCreateView(CreateView):
    model = UserSite
    form_class = UserSiteForm
    template_name = 'user_site_create.html'  
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        form.instance.user = self.request.user  
        return super().form_valid(form)
        
    
class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context["user_links"] = UserSite.objects.filter(user=self.request.user).all()
        return context
    

class ProxyView(View):
    
    def get(self, request, user_site_name, path):
        user_site = UserSite.objects.get(user=request.user, name=user_site_name)
        
        url = f'{user_site.url}/{path}'
        response = requests.get(url)
        content = response.content
        
        soup = BeautifulSoup(content, 'html.parser')
        for link in soup.find_all('a'):
            if 'href' in link.attrs:
                href = link.attrs['href']
                if not href.startswith('http'): 
                    link.attrs['href'] = f'/proxy/{user_site_name}/{href}'
        
        modified_content = str(soup)

        stats, created = TrafficStats.objects.get_or_create(
            user=request.user,
            site=user_site,
            defaults={'visits': 1, 'data_transferred': len(content)}
        )
        if not created:
            stats.visits += 1
            stats.data_transferred += len(content)
            stats.save()
        
        return HttpResponse(modified_content)
    

     